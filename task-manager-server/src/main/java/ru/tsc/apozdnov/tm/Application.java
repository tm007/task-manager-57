package ru.tsc.apozdnov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.apozdnov.tm.component.Bootstrap;
import ru.tsc.apozdnov.tm.config.ContextConfig;

public final class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final AnnotationConfigApplicationContext cntx =
                new AnnotationConfigApplicationContext(ContextConfig.class);
        cntx.registerShutdownHook();
        @NotNull final Bootstrap bootstrap = cntx.getBean(Bootstrap.class);
        bootstrap.run();
    }

}
